var areaFilling = function(){

  var canvas = document.getElementById("canvas");
  var ctx = canvas.getContext("2d");

  var area = {
    size: 5, /*Размер одной стороны в секторах*/
    step: 100, /*Размер сектора*/
    marginRight: 400 /*Отсуп справа от области, где будут находиться
    неустановленные фигуры*/
  };

  canvas.width = area.size*area.step+area.marginRight;
  canvas.height = area.size*area.step;
  canvas.style.backgroundColor = 'white';

  /*Размеры области размещения фигур*/
  placeWidth = canvas.width-area.marginRight;
  placeHeight = area.size*area.step;

  var rectSelect = null; /*Хранит индекс перетаскиваемого элемента*/
  var counter=0; /*Считает количество фигур, помещенных на холст*/

  var rects = []; /*Массив представления о заполнении*/
  var lockedPl = []; /*Массив занятых секторов*/
  var rectsImagine = [];
  canvas.onmousedown = mouseDown;
  canvas.onmouseup = mouseUp;
  canvas.onmousemove = mouseMove;

/*11213232222122*/
/*21111121323221211121*/
  createRects('11213232222122');
  getImagin();
  drawGrid();
  draw();

  /*Принимает в JSON параметры Canvas*/
  function setArea(data){
    var input;
    input = JSON.parse(data);
    area.size = input.size;
    area.step = input.step;
    canvas.style.backgroundColor = input.bgColor;
    console.log(input);
  }

  function createRects(argString){

    var argsNum = argString.length;
    if(argsNum%2 != 0) {
      console.log("error of input");
      return;
    }
    for(var i = 0, id=0; i<argsNum; i+=2, id++){
      rects.push({id: id, x: canvas.width-350,y: 100,w: argString[i]*area.step,
        h: argString[(i+1)]*area.step });
      }
  }

  function sortRects(rects){
    for(var i = 0; i<rects.length; i++){
      if(rects[i].w < rects[i].h){
        swapRectWH(rects, i);
      }
    }
    function compareWidth(rectA, rectB) {
      if(rectA.w == rectB.w) return rectB.h - rectA.h;
      return rectB.w - rectA.w;
    }
    rects.sort(compareWidth);
  }

  function swapRectWH(array, index){
    var temp = 0;

    temp = array[index].w;
    array[index].w = array[index].h;
    array[index].h = temp;
  }

  /*Функция заполняет массив lockedPl координатами занятых ячеек и отправляет
  true, если ячейки заняты - false*/
  function takeArea(x, y, index, readOnly){
    if(x < (placeWidth)){
      var countInputs = 0;
      for(var i = x; i < x+rects[index].w; i+= area.step){
        for(var j = y; j < y+rects[index].h; j+= area.step){
          for(var k = 0; k<lockedPl.length; k++){
              if((counter>0) && (lockedPl[k].x === i) && (lockedPl[k].y === j)){
                if(!readOnly){
                lockedPl.splice(((lockedPl.length)-countInputs), countInputs);
              }
              return false;
            } else if((x+rects[index].w > placeWidth) || (y+rects[index].h>placeHeight)) {
              return false;
            }
          }
          if(!readOnly){ lockedPl.push({ x: i, y: j }); }
          countInputs++;
        }
      }

    }
    return true;
  }

  /*Функция проверяет, заполнена ли область фигурами. Сравнивает площадь области
  с площадью, занимаемой фигурами, находящимися в пределах области. Корректно
  работать будет только при условии невозможности наложения фигур друг на друга*/
  function isFilled(){
    var areaOfShapes = 0;

    for(var i=0, length = rects.length; i < length; i++){
      if(rects[i].x < placeWidth){
        areaOfShapes += rects[i].w * rects[i].h;
      }
    }
    if(areaOfShapes >= Math.pow((area.step*area.size),2)){
      return true;
    }
    return false;
  }

  function drawGrid(){

    var pen = {
      x: 0,
      y: canvas.height
    };

    for(var i=0; i <= area.size; i++){
      ctx.moveTo(pen.x, 0);
      ctx.lineTo(pen.x, pen.y);
      pen.x+=(area.step);
    }

    pen.x=placeWidth, pen.y = 0;

    for(var i=0; i <= area.size; i++){
      ctx.moveTo(0, pen.y);
      ctx.lineTo(pen.x, pen.y);
      pen.y+=(area.step);
    }
    ctx.strokeStyle = "#888";
    ctx.stroke();
  }

  /*Функция заполняет массив прямоугольников. В качестве аргументов
  принимает строку, состоящую из числовой последовательности, характеризующей
  размер фигур(измеряется в секторах) createRects('width, height... etc')*/
  function draw(){

    ctx.clearRect(0,0,canvas.width,canvas.height);
    drawGrid();
    for(var i=0, length = rects.length; i<length; i++){
      rects[i].x>(placeWidth)?ctx.fillStyle=
          "rgba(54, 176, 251, 0.5)":ctx.fillStyle="rgba(54, 176, 251, 1)";
      ctx.fillRect(rects[i].x, rects[i].y, rects[i].w, rects[i].h);
    }
  }

  function mouseDown(e){

    for(var i=0, length = rects.length; i<length; i++){
      if(((e.offsetX>rects[i].x) && (e.offsetX<rects[i].x+rects[i].w)) &&
       ((e.offsetY>rects[i].y) && (e.offsetY<rects[i].y+rects[i].h)) && (rects[i].x > placeWidth)){
        rectSelect = i;

      }
    }
  }

  function mouseMove(e){

    if(rectSelect!=null){
      rects[rectSelect].x = e.offsetX;
      rects[rectSelect].y = e.offsetY;
    }
    drawGrid();
    draw();
  }

  function getImagin(){

    sortRects(rects);
    for(var i = 0; i < rects.length; i++){
      stop = false;
      for(var y = 0; y < placeHeight && !stop; y+=area.step){
        for(var x = 0; x < placeWidth && !stop; x+=area.step){
          if(takeArea(x, y, i, false)){
            rectsImagine.push({id: rects[i].id, x: x, y: y});
            counter++;
            stop = true;
          }
        }
      }
      if(!stop){
        swapRectWH(rects, i);
        for(var y = 0; y < placeHeight && !stop; y+=area.step){
          for(var x = 0; x < placeWidth && !stop; x+=area.step){
            if(takeArea(x, y, i, false)){
              rectsImagine.push({id: rects[i].id, x: x, y: y});
              counter++;
              stop = true;
            }
          }
        }
      }
    }
  }

  function mouseUp(e){

    if(rectSelect!=null && (rects[rectSelect].x < placeWidth)){
      for(var i = 0; i<rectsImagine.length; i++){
        if(rects[rectSelect].id === rectsImagine[i].id){
          rects[rectSelect].x = rectsImagine[i].x;
          rects[rectSelect].y = rectsImagine[i].y;
          rectsImagine.splice(i, 1);
          break;
        } else {
          rects[rectSelect].x = canvas.width - 350;
          rects[rectSelect].y = 100;
        }
      }
    }
    isFilled()?console.log("Область заполнена"):console.log("Область ещё не заполнена");
    rectSelect = null;
  }

  return{
    setArea: setArea
  };

}();
